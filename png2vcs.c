
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include <gd.h>
#include <gdfontl.h>

static const char _helpmessage[] =
"%s: image to Atari 2600 VCS code converter\n"
"\n"
"-0 #:\tthe VCS palette index of 0 bit (default $00)\n"
"-a $:\t'fill' pseudo opcode for padding (default '.res')\n"
"-b $:\t'byte' pseudo opcode (default '.byte')\n"
"-c  :\tadditional color information\n"
"-f  :\tforward outout (default is bottom-up)\n"
"-h #:\theight (default full image)\n"
"-i $:\tinput file (default: stdin)\n"
"-l $:\tlabel base (will be appended with index number)\n"
"-m $:\tmode (see below)\n"
"-n  :\tuse NTSC palette instead of PAL\n"
"-o $:\toutput file (default stdout)\n"
"-p #:\tpage boundry, data will be padded to prevent page crossing\n"
"-r  :\trevert image\n"
"-w #:\twidth (default full image)\n"
"-x #:\tx position of start (default 0)\n"
"-y #:\ty position of start (default 0)\n"
"\n\b"
"modes are: playfield,reduced playfield,mirrored playfield,\n"
"           sprite,charset,image with palette,dump palette\n"
"\n";

typedef enum {
   MODE_NONE = 0,
   MODE_SPRITE,
   MODE_PLAYFIELD,
   MODE_PLAYFIELD_MIRRORED,
   MODE_PLAYFIELD_REDUCED,
   MODE_CHARSET,
   MODE_PALETTE_IMAGE,
   MODE_DUMP_PALETTE,
} convmode_t;

/* 
  TODO: convert "rainbow color image"
  int gdImageColorAllocate(gdImagePtr im, int r, int g, int b)
  gdImagePaletteCopy(gdImagePtr dst, gdImagePtr src)
 */

typedef struct { unsigned char r; unsigned char g; unsigned char b; } rgb;

/* palettes taken from Stella */

static const rgb palette_pal[128] = {

{ 0, 0, 0 }, { 40, 40, 40 }, { 80, 80, 80 }, { 116, 116, 116 },
{ 148, 148, 148 }, { 180, 180, 180 }, { 208, 208, 208 }, { 236, 236, 236 },

{ 0, 0, 0 }, { 40, 40, 40 }, { 80, 80, 80 }, { 116, 116, 116 },
{ 148, 148, 148 }, { 180, 180, 180 }, { 208, 208, 208 }, { 236, 236, 236 },

{ 128, 88, 0 }, { 148, 112, 32 }, { 168, 132, 60 }, { 188, 156, 88 },
{ 204, 172, 112 }, { 220, 192, 132 }, { 236, 208, 156 }, { 252, 224, 176 },

{ 68, 92, 0 }, { 92, 120, 32 }, { 116, 144, 60 }, { 140, 172, 88 },
{ 160, 192, 112 }, { 176, 212, 132 }, { 196, 232, 156 }, { 212, 252, 176 },

{ 112, 52, 0 }, { 136, 80, 32 }, { 160, 104, 60 }, { 180, 132, 88 },
{ 200, 152, 112 }, { 220, 172, 132 }, { 236, 192, 156 }, { 252, 212, 176 },

{ 0, 100, 20 }, { 32, 128, 52 }, { 60, 152, 80 }, { 88, 176, 108 },
{ 112, 196, 132 }, { 132, 216, 156 }, { 156, 232, 180 }, { 176, 252, 200 },

{ 112, 0, 20 }, { 136, 32, 52 }, { 160, 60, 80 }, { 180, 88, 108 },
{ 200, 112, 132 }, { 220, 132, 156 }, { 236, 156, 180 }, { 252, 176, 200 },

{ 0, 92, 92 }, { 32, 116, 116 }, { 60, 140, 140 }, { 88, 164, 164 },
{ 112, 184, 184 }, { 132, 200, 200 }, { 156, 220, 220 }, { 176, 236, 236 },

{ 112, 0, 92 }, { 132, 32, 116 }, { 148, 60, 136 }, { 168, 88, 156 },
{ 180, 112, 176 }, { 196, 132, 192 }, { 208, 156, 208 }, { 224, 176, 224 },

{ 0, 60, 112 }, { 28, 88, 136 }, { 56, 116, 160 }, { 80, 140, 180 },
{ 104, 164, 200 }, { 124, 184, 220 }, { 144, 204, 236 }, { 164, 224, 252 },

{ 88, 0, 112 }, { 108, 32, 136 }, { 128, 60, 160 }, { 148, 88, 180 },
{ 164, 112, 200 }, { 180, 132, 220 }, { 196, 156, 236 }, { 212, 176, 252 },

{ 0, 32, 112 }, { 28, 60, 136 }, { 56, 88, 160 }, { 80, 116, 180 },
{ 104, 136, 200 }, { 124, 160, 220 }, { 144, 180, 236 }, { 164, 200, 252 },

{ 60, 0, 128 }, { 84, 32, 148 }, { 108, 60, 168 }, { 128, 88, 188 },
{ 148, 112, 204 }, { 168, 132, 220 }, { 184, 156, 236 }, { 200, 176, 252 },

{ 0, 0, 136 }, { 32, 32, 156 }, { 60, 60, 176 }, { 88, 88, 192 },
{ 112, 112, 208 }, { 132, 132, 224 }, { 156, 156, 236 }, { 176, 176, 252 },

{ 0, 0, 0 }, { 40, 40, 40 }, { 80, 80, 80 }, { 116, 116, 116 },
{ 148, 148, 148 }, { 180, 180, 180 }, { 208, 208, 208 }, { 236, 236, 236 },

{ 0, 0, 0 }, { 40, 40, 40 }, { 80, 80, 80 }, { 116, 116, 116 },
{ 148, 148, 148 }, { 180, 180, 180 }, { 208, 208, 208 }, { 236, 236, 236 } };

static const rgb palette_ntsc[128] = {

{ 0, 0, 0 }, { 74, 74, 74 }, { 111, 111, 111 }, { 142, 142, 142 },
{ 170, 170, 170 }, { 192, 192, 192 }, { 214, 214, 214 }, { 236, 236, 236 },

{ 72, 72, 0 }, { 105, 105, 15 }, { 134, 134, 29 }, { 162, 162, 42 },
{ 187, 187, 53 }, { 210, 210, 64 }, { 232, 232, 74 }, { 252, 252, 84 },

{ 124, 44, 0 }, { 144, 72, 17 }, { 162, 98, 33 }, { 180, 122, 48 },
{ 195, 144, 61 }, { 210, 164, 74 }, { 223, 183, 85 }, { 236, 200, 96 },

{ 144, 28, 0 }, { 163, 57, 21 }, { 181, 83, 40 }, { 198, 108, 58 },
{ 213, 130, 74 }, { 227, 151, 89 }, { 240, 170, 103 }, { 252, 188, 116 },

{ 148, 0, 0 }, { 167, 26, 26 }, { 184, 50, 50 }, { 200, 72, 72 },
{ 214, 92, 92 }, { 228, 111, 111 }, { 240, 128, 128 }, { 252, 144, 144 },

{ 132, 0, 100 }, { 151, 25, 122 }, { 168, 48, 143 }, { 184, 70, 162 },
{ 198, 89, 179 }, { 212, 108, 195 }, { 224, 124, 210 }, { 236, 140, 224 },

{ 80, 0, 132 }, { 104, 25, 154 }, { 125, 48, 173 }, { 146, 70, 192 },
{ 164, 89, 208 }, { 181, 108, 224 }, { 197, 124, 238 }, { 212, 140, 252 },

{ 20, 0, 144 }, { 51, 26, 163 }, { 78, 50, 181 }, { 104, 72, 198 },
{ 127, 92, 213 }, { 149, 111, 227 }, { 169, 128, 240 }, { 188, 144, 252 },

{ 0, 0, 148 }, { 24, 26, 167 }, { 45, 50, 184 }, { 66, 72, 200 },
{ 84, 92, 214 }, { 101, 111, 228 }, { 117, 128, 240 }, { 132, 144, 252 },

{ 0, 28, 136 }, { 24, 59, 157 }, { 45, 87, 176 }, { 66, 114, 194 },
{ 84, 138, 210 }, { 101, 160, 225 }, { 117, 181, 239 }, { 132, 200, 252 },

{ 0, 48, 100 }, { 24, 80, 128 }, { 45, 109, 152 }, { 66, 136, 176 },
{ 84, 160, 197 }, { 101, 183, 217 }, { 117, 204, 235 }, { 132, 224, 252 },

{ 0, 64, 48 }, { 24, 98, 78 }, { 45, 129, 105 }, { 66, 158, 130 },
{ 84, 184, 153 }, { 101, 209, 174 }, { 117, 231, 194 }, { 132, 252, 212 },

{ 0, 68, 0 }, { 26, 102, 26 }, { 50, 132, 50 }, { 72, 160, 72 },
{ 92, 186, 92 }, { 111, 210, 111 }, { 128, 232, 128 }, { 144, 252, 144 },

{ 20, 60, 0 }, { 53, 95, 24 }, { 82, 126, 45 }, { 110, 156, 66 },
{ 135, 183, 84 }, { 158, 208, 101 }, { 180, 231, 117 }, { 200, 252, 132 },

{ 48, 56, 0 }, { 80, 89, 22 }, { 109, 118, 43 }, { 136, 146, 62 },
{ 160, 171, 79 }, { 183, 194, 95 }, { 204, 216, 110 }, { 224, 236, 124 },

{ 72, 44, 0 }, { 105, 77, 20 }, { 134, 106, 38 }, { 162, 134, 56 },
{ 187, 159, 71 }, { 210, 182, 86 }, { 232, 204, 99 }, { 252, 224, 112 } };

typedef struct config {
   gdImagePtr  im;
   FILE        *f;
   convmode_t  mode;
   int         revert;
   int         colorscan;
   int         pad;
   int         forward;
   int         height;
   int         width;
   int         xstart;
   int         ystart;
   int         zero;
   int         ntsc;
   const char  *infile;
   const char  *outfile;
   const char  *label;
   const char  *byte;
   const char  *res;
   int         index;
   int         pos;
} config_t;

static const char _byte[] = "   .byte";
static const char _res[]  = "   .res";

static unsigned char mirrorbyte( unsigned char in )
{
   unsigned char out = 0;
   int i;
   for( i = 0; i < 8; ++i )
   {
      out <<= 1;
      out |= (in & 1);
      in >>= 1;
   }
   return out;
}


static void dumppalette( config_t *c )
{
   int palsize = sizeof(palette_pal)/sizeof(palette_pal[0]);
   const rgb *palette = c->ntsc ? palette_ntsc : palette_pal;
   int i;

   fprintf( c->f, "GIMP Palette\nName: Atari 2600 VCS Palette PAL\nColumns: 8\n#\n" );
   for( i = 0; i < palsize; ++i )
   {
      int grey = (299 * palette[i].r + 587 * palette[i].g + 114 * palette[i].b) / 1000;
      fprintf( c->f, "%3d %3d %3d\t$%02X bright:%3d\n",
               palette[i].r, palette[i].g, palette[i].b, i << 1, grey );
   }
}


static void setpalette( config_t *c )
{
   if( c->im )
   {
      int i;
      int palsize = sizeof(palette_pal)/sizeof(palette_pal[0]);
      const rgb *palette = c->ntsc ? palette_ntsc : palette_pal;

      for( i = 0; i < palsize; ++i )
      {
         gdImageColorAllocate( c->im, palette[i].r, palette[i].g, palette[i].b );
      }
   }
}


static void dumpimage( config_t *c )
{
   FILE *f;

   c->im = gdImageCreate( c->width, c->height );

   setpalette( c );
   f = fopen( c->outfile, "wb" );
   if( f )
   {
      gdImagePng( c->im, f );
   }
   else
   {
      perror( "opening image file" );
      exit( 1 );
   }
   gdImageDestroy( c->im );
   c->im = NULL;
}


static void createoutput( config_t *c )
{
   if( c->outfile )
   {
      c->f = fopen( c->outfile, "wb" );
      if( !c->f )
      {
         fprintf( stderr, "can't open output file '%s': %s\n", c->outfile, strerror(errno) );
         exit( 1 );
      }
   }
   else
   {
      c->f = stdout;
   }
}


static void cleanup( config_t *c )
{
   if( c->f && (c->f != stdout) )
   {
      fclose( c->f );
      c->f = NULL;
   }

   if( c->im )
   {
      gdImageDestroy( c->im );
      c->im = NULL;
   }
}


static void help( char *argv0, int exitcode )
{
   fprintf( exitcode ? stderr : stdout, _helpmessage, argv0 );
   exit( exitcode );
}


void writechars( config_t *c, int yoffset, unsigned char ch )
{
   int xoffset, x, y;
   int realy;
   char buffer[33];
   char *b = buffer;

   for( xoffset = 0; xoffset < 256; xoffset += 8 )
   {
      for( y = 0; y < 8; ++y )
      {
         int value = 0;
         for( x = 0; x < 8; ++x )
         {
            if( c->forward )
            {
               realy = y;
            }
            else
            {
               realy = c->height - y - 1;
            }
            if( c->im->trueColor )
            {
               if( c->im->tpixels[c->ystart + yoffset + realy][c->xstart + xoffset + x] != c->zero )
               {
                  value |= 1 << (7-x);
               }
            }
            else
            {
               if( c->im->pixels[c->ystart + yoffset + realy][c->xstart + xoffset + x] != c->zero )
               {
                  value |= 1 << (7-x);
               }
            }
         }
         if( c->revert )
         {
            value = ~value;
         }
         sprintf( b, "$%02x,", value );
      }
      buffer[32] = '\0';
      b = buffer;
      switch( ch )
      {
      case 0x20:
      case 0x7F:
         fprintf( c->f, "%s %s ; $%02x\n", c->byte, b, ch++ );
         break;
      default:
         fprintf( c->f, "%s %s ; '%c'\n", c->byte, b, ch++ );
         break;
      }
   }
}


static void loadimage( config_t *c )
{
   gdImagePtr im;
   FILE *f;
   int x,y;
   if( c->infile )
   {
      f = fopen( c->infile, "rb" );
      if( !f )
      {
         perror( "error loading file" );
         exit( 1 );
      }
      im = gdImageCreateFromPng( f );
      fclose( f );
      if( !im )
      {
         fprintf( stderr, "could not load image '%s'\n", c->infile );
         exit( 1 );
      }
   }
   else
   {
      im = gdImageCreateFromPng( stdin );
      if( !im )
      {
         fprintf( stderr, "could not load image from stdin\n" );
         exit( 1 );
      }
   }

   if( c->height <= 0 )
   {
      c->height = im->sy - c->ystart;
   }
   if( c->width <= 0 )
   {
      c->width = im->sx - c->xstart;
   }

   c->im = gdImageCreate( c->width, c->height );
   setpalette( c );
   for( y = 0; y < c->height; ++y )
   {
      for( x = 0; x < c->width; ++x )
      {
         int tc = gdImageGetTrueColorPixel( im, c->xstart + x, c->ystart + y );
         int ci = gdImageColorClosest( c->im,
                                       (tc & 0xff0000) >> 16,
                                       (tc & 0xff00) >> 8,
                                       (tc & 0xff) );
         gdImageSetPixel( c->im, x, y, ci );
      }
   }
   gdImageDestroy( im );
   f = fopen( "debug.png", "wb" );
   gdImagePng( c->im, f );
   fclose( f );

#if 1
   /* workaround: remove usage of xstart and ystart later */
   c->xstart = 0;
   c->ystart = 0;
#endif
}


static void writepad( config_t *c )
{
   if( c->pos == c->pad )
   {
      c->pos = 0;
   }
   if( c->pad > c->height )
   {
      if( (c->pos + c->height) > c->pad )
      {
         fprintf( c->f, "%s %d\n", c->res, c->pad - c->pos );
         c->pos = 0;
      }
   }
}


static void writechunk( config_t *c, int offset, int bits, int mirror )
{
   char buffer[65] = {0};
   char *b = buffer;
   int bufferelements = 0;
   int line;
   int realy;

   writepad( c );
   fprintf( c->f, "%s%d:\n", c->label, (c->index)++ );
   for( line = 0; line < c->height; ++line )
   {
      int bit;
      unsigned char value = 0;
      for( bit = 0; bit < bits; ++bit )
      {
         if( c->forward )
         {
            realy = line;
         }
         else
         {
            realy = c->height - line - 1;
         }
         if( (c->xstart + offset + bit) < c->im->sx )
         {
            if( c->im->trueColor )
            {
               if( c->im->tpixels[c->ystart + realy][c->xstart + offset + bit] != c->zero )
               {
                  value |= 1 << (7-bit);
               }
            }
            else
            {
               if( c->im->pixels[c->ystart + realy][c->xstart + offset + bit] != c->zero )
               {
                  value |= 1 << (7-bit);
               }
            }
         }
      }
      if( c->revert )
      {
         value = ~value;
      }
      if( mirror )
      {
         value = mirrorbyte( value );
      }
      if( (c->mode == MODE_PLAYFIELD) && (bits == 4) )
      {
         value <<= 4;
      }
      sprintf( b, "$%02x,", value );
      b += 4;
      if( ++bufferelements > 15 )
      {
         bufferelements = 0;
         *(--b) = 0; /* remove last ',' */
         b = buffer;
         fprintf( c->f, "%s %s\n", c->byte, b );
      }
   }

   if( bufferelements )
   {
      *(--b) = 0; /* remove last ',' */
      b = buffer;
      fprintf( c->f, "%s %s\n", c->byte, b );
   }

   c->pos += c->height;
}


static void writecolors( config_t *c )
{
   char buffer[65] = {0};
   char *b = buffer;
   int bufferelements = 0;
   int line;
   int realy;

   if( !c->colorscan )
   {
      return;
   }

   writepad( c );
   fprintf( c->f, "%s_c:\n", c->label );
   for( line = 0; line < c->height; ++line )
   {
      int x;
      int color = c->zero;

      if( c->forward )
      {
         realy = line;
      }
      else
      {
         realy = c->height - line - 1;
      }

      for( x = 0; (x < c->width) && (color == c->zero); ++x )
      {
         color = c->im->pixels[c->ystart + realy][c->xstart + x];
      }

      sprintf( b, "$%02x,", color << 1 );
      b += 4;

      if( ++bufferelements > 15 )
      {
         bufferelements = 0;
         *(--b) = 0; /* remove last ',' */
         b = buffer;
         fprintf( c->f, "%s %s\n", c->byte, b );
      }
   }

   if( bufferelements )
   {
      *(--b) = 0; /* remove last ',' */
      b = buffer;
      fprintf( c->f, "%s %s\n", c->byte, b );
   }

   c->pos += c->height;
}


static void writesprite( config_t *c )
{
   int x;
   for( x = 0; x < c->width; x += 8 )
   {
      int width = 8;
      if( (c->width - x) < 8 )
      {
         width = (c->width - x);
      }
      writechunk( c, x, width, 0 );
   }
   writecolors( c );
}


static void writeplayfield( config_t *c )
{
   writechunk( c,  0, 4, 1 );
   writechunk( c,  4, 8, 0 );
   writechunk( c, 12, 8, 1 );
   if( c->width > 20 )
   {
      writechunk( c, 20, 4, 1 );
      writechunk( c, 24, 8, 0 );
      writechunk( c, 32, 8, 1 );
   }
   writecolors( c );
}


static void writemirroredplayfield( config_t *c )
{
   writechunk( c,  0, 4, 1 );
   writechunk( c,  4, 8, 0 );
   writechunk( c, 12, 8, 1 );
   if( c->width > 20 )
   {
      writechunk( c, 20, 8, 0 );
      writechunk( c, 28, 8, 1 );
      writechunk( c, 36, 4, 0 );
   }
   writecolors( c );
}


static void writereducedplayfield( config_t *c )
{
   int x;
   int y;
   unsigned char tmppixels[4];
   int           tmptpixels[4];

   if( c->im->trueColor )
   {
      for( y = 0; y < c->height; ++y )
      {
         for( x = 0; x < 4; ++x )
         {
            tmptpixels[x] = c->im->tpixels[y][x+20];
         }
         for( x = 15; x >= 0; --x )
         {
            c->im->tpixels[y][x+8] = c->im->tpixels[y][x+4];
         }
         for( x = 0; x < 4; ++x )
         {
            c->im->tpixels[y][x+4] = tmptpixels[x];
         }
      }
   }
   else
   {
      for( y = 0; y < c->height; ++y )
      {
         for( x = 0; x < 4; ++x )
         {
            tmppixels[x] = c->im->pixels[y][x+20];
         }
         for( x = 15; x >= 0; --x )
         {
            c->im->pixels[y][x+8] = c->im->pixels[y][x+4];
         }
         for( x = 0; x < 4; ++x )
         {
            c->im->pixels[y][x+4] = tmppixels[x];
         }
      }
   }
   writechunk( c,  0, 8, 1 );
   writechunk( c,  8, 8, 0 );
   writechunk( c, 16, 8, 1 );
   writechunk( c, 24, 8, 0 );
   writechunk( c, 32, 8, 1 );
   writecolors( c );
}


int main( int argc, char *argv[] )
{
   config_t c;
   int opt;
   int fail = 0;

   memset( &c, 0, sizeof(c) );
   c.byte = _byte;
   c.res  = _res;

   if( argc < 2 )
   {
      help( argv[0], 0 );
   }

   while( ( opt = getopt(argc, argv, "0:a:b:cfh:i:l:m:no:p:rw:x:y:") ) != -1 )
   {
      switch( opt )
      {
      case '0': /* palette zero entry */
         c.zero = strtol( optarg, 0, 0 ) >> 1;
         break;
      case 'a': /* fill opcode */
         c.res = optarg;
         break;
      case 'b': /* byte opcode */
         c.byte = optarg;
         break;
      case 'c': /* scan colors */
         c.colorscan = 1;
         break;
      case 'f':
         c.forward = 1;
         fail = 1;
         fprintf( stderr, "forward output not implemented yet" );
         break;
      case 'h': /* height */
         c.height = strtol( optarg, 0, 0 );
         break;
      case 'i': /* image (input) file */
         c.infile = optarg;
         break;
      case 'l': /* label base */
         c.label = optarg;
         break;
      case 'm': /* mode: playfield, sprite */
         switch( tolower(*optarg) )
         {
         case 'c':
            c.mode = MODE_CHARSET;
            break;
         case 'd':
            c.mode = MODE_DUMP_PALETTE;
            break;
         case 'i':
            c.mode = MODE_PALETTE_IMAGE;
            break;
         case 'm':
            c.mode = MODE_PLAYFIELD_MIRRORED;
            break;
         case 'p':
            c.mode = MODE_PLAYFIELD;
            break;
         case 'r':
            c.mode = MODE_PLAYFIELD_REDUCED;
            break;
         case 's':
            c.mode = MODE_SPRITE;
            break;
         default:
            fail = 1;
            fprintf( stderr, "unknown mode: %c\n", tolower(*optarg) );
         }
         break;
      case 'n': /* use ntsc palette */
         c.ntsc = 1;
         break;
      case 'o': /* output file */
         c.outfile = optarg;
         break;
      case 'p': /* pad */
         c.pad = strtol( optarg, 0, 0 );
         break;
      case 'r': /* reverse image data */
         c.revert = 1;
         break;
      case 'w': /* width */
         c.width = strtol( optarg, 0, 0 );
         break;
      case 'x': /* starting xpos */
         c.xstart = strtol( optarg, 0, 0 );
         break;
      case 'y': /* starting ypos */
         c.ystart = strtol( optarg, 0, 0 );
         break;
      default:
         fprintf( stderr, "unknown option: %c\n", opt );
         fail = 1;
      }
   }

   /* sanity checks */
   switch( c.mode )
   {
   case MODE_CHARSET:
      fail = 1;
      fprintf( stderr, "charset not implemented yet" );
      break;
   case MODE_DUMP_PALETTE:
      break;
   case MODE_PALETTE_IMAGE:
      if( !c.outfile )
      {
         fail = 1;
         fprintf( stderr, "creaing palette image file needs an output filename\n" );
      }
      if( c.width < 1 )
      {
         c.width = 1;
      }
      if( c.height < 1 )
      {
         c.height = 1;
      }
      break;
   case MODE_PLAYFIELD:
   case MODE_PLAYFIELD_MIRRORED:
   case MODE_PLAYFIELD_REDUCED:
   case MODE_SPRITE:
      if( !c.infile )
      {
         fail = 1;
         fprintf( stderr, "creating source code data needs an input file\n" );
      }
      if( !c.label )
      {
         fail = 1;
         fprintf( stderr, "creating source code data needs a label base\n" );
      }
      if( !c.outfile )
      {
          c.f = stdout;
      }
      break;
   default:
      fail = 1;
      fprintf( stderr, "mode not set\n" );
   }

   if( fail )
   {
      fprintf( stderr, "wrong usage\n" );
      help( argv[0], fail );
   }

   createoutput( &c );

   switch( c.mode )
   {
   case MODE_SPRITE:
      loadimage( &c );
      writesprite( &c );
      break;
   case MODE_PLAYFIELD_REDUCED:
      loadimage( &c );
      writereducedplayfield( &c );
      break;
   case MODE_PLAYFIELD_MIRRORED:
      loadimage( &c );
      writemirroredplayfield( &c );
      break;
   case MODE_PLAYFIELD:
      loadimage( &c );
      writeplayfield( &c );
      break;
   case MODE_CHARSET:
      loadimage( &c );
      fprintf( c.f, "%s:\n", c.label );
      writechars( &c, 32, 0 );
      if( c.height > 8 )
      {
         writechars( &c, 64, 0 );
      }
      if( c.height > 16 )
      {
         writechars( &c, 96, 0 );
      }
      break;
   case MODE_DUMP_PALETTE:
      dumppalette( &c );
      break;
   case MODE_PALETTE_IMAGE:
      dumpimage( &c );
      break;
   default:
      fprintf( stderr, "%s:%d:internal error", __FILE__, __LINE__ );
      exit( 1 );
   }

   cleanup( &c );
   return 0;
}
