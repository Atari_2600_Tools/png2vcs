# png2vcs #

## image to Atari 2600 VCS code converter ##

```
Options:

./png2vcs: image to Atari 2600 VCS code converter

-0 #:	the VCS palette index of 0 bit (default $00)
-a $:	'fill' pseudo opcode for padding (default '.res')
-b $:	'byte' pseudo opcode (default '.byte')
-c  :	additional color information
-f  :	forward outout (default is bottom-up)
-h #:	height (default full image)
-i $:	input file (default: stdin)
-l $:	label base (will be appended with index number)
-m $:	mode (see below)
-n  :	use NTSC palette instead of PAL
-o $:	output file (default stdout)
-p #:	page boundry, data will be padded to prevent page crossing
-r  :	revert image
-w #:	width (default full image)
-x #:	x position of start (default 0)
-y #:	y position of start (default 0)

modes are: playfield,reduced playfield,mirrored playfield,
           sprite,charset,image with palette,dump palette
```
All modes can be abbreviated using the first letter.

When using "reduced playfield" mode, the low nibble holds "left" PF0 data,
the high nibble "right" PF0.

`-x` and `-y` can take negative parameters to "move" the image from the border.
When using `-x` and `-y` the usage of `-w` and `-h` is suggested as those
values do not get adjusted.

