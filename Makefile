
CC = gcc

# 
CFLAGS += -Os -Wall

# get from pkg-config
CFLAGS += $(shell pkg-config --cflags gdlib)
LDFLAGS += $(shell pkg-config --libs gdlib)

# specify by hand (static lib configuration)
#CFLAGS +=
#LDFLAGS += -lgd -lpng -lz

progs = png2vcs

all: $(progs)

clean:
	rm -f $(progs)

png2vcs: png2vcs.c
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

